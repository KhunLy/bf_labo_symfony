/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */


// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

import 'bootstrap/dist/js/bootstrap.min';

// start the Stimulus application
import './bootstrap';

import 'select2/dist/css/select2.min.css';
import 'select2';


$('.select2').select2({
    width: '100%',
    theme: 'bootstrap-5'
});

$('.form-range').on('input', e => {
   const valueSpan = $(e.target).siblings('.range-value');
   valueSpan.text(e.target.value);
});

