<?php

namespace App\Tests\service;

use App\Entity\Gender;
use App\Entity\Player;
use App\Entity\Tournament;
use App\Entity\TournamentCategory;
use App\Service\TournamentService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TournamentServiceTest extends KernelTestCase
{
    /** @var Player */
    private $player;

    /** @var Tournament */
    private $tournament;

    /** @var ContainerInterface */
    private $container;

    /** @var TournamentService */
    private $tournamentService;


    public function beforeEach()
    {
        /** need it to get TournamentService */
        self::bootKernel();
        $this->container = static::getContainer();
        $this->tournamentService = $this->container->get(TournamentService::class);

        $tomorrow = (new \DateTime())->add(new \DateInterval('P1D'));

        $this->tournament = new Tournament();
        $this->tournament->setDate($tomorrow);
        $this->tournament->setMax(3);
        $this->tournament->setGenders(['Female']);
        $this->tournament->setCategories(['Senior']);

        $this->player = new Player();
        $this->player->setElo(1200);
        $this->player->setGender(Gender::FEMALE);
        $twentyDate = ((new \DateTime())->add(new \DateInterval('P1D')))
            ->sub(new \DateInterval('P20Y'));
        $this->player->setBirthDate($twentyDate);
    }

    public function testCanPlayReturnsTrue() {
        $this->beforeEach();

        $result = $this->tournamentService->canPlay($this->player, $this->tournament);
        $this->assertTrue($result);
    }

    public function testCanPlayIfNotConnectedReturnsFalse() {

        $this->beforeEach();

        $result = $this->tournamentService->canPlay(null, $this->tournament);
        $this->assertFalse($result);
    }

    public function testCanPlayIfFullReturnsFalse() {

        $this->beforeEach();

        $this->tournament->addPlayer(new Player());
        $this->tournament->addPlayer(new Player());
        $this->tournament->addPlayer(new Player());

        $result = $this->tournamentService->canPlay($this->player, $this->tournament);
        $this->assertFalse($result);
    }

    public function testCanPlayIfPlayerAlreadyAddReturnsFalse() {
        $this->beforeEach();

        $this->tournament->addPlayer($this->player);

        $result = $this->tournamentService->canPlay($this->player, $this->tournament);
        $this->assertFalse($result);
    }

    public function testCanPlayIfTournamentDateExpiresReturnsFalse() {
        $this->beforeEach();

        $yesterday = (new \DateTime())->sub(new \DateInterval('P1D'));
        $this->tournament->setDate($yesterday);

        $result = $this->tournamentService->canPlay($this->player, $this->tournament);
        $this->assertFalse($result);
    }

    public function testCanPlayIfPlayerEloToHighReturnsFalse() {
        $this->beforeEach();

        $this->tournament->setEloMax(1000);

        $result = $this->tournamentService->canPlay($this->player, $this->tournament);
        $this->assertFalse($result);
    }

    public function testCanPlayIfPlayerEloToLowReturnsFalse() {
        $this->beforeEach();

        $this->tournament->setEloMin(1300);

        $result = $this->tournamentService->canPlay($this->player, $this->tournament);
        $this->assertFalse($result);
    }

    public function testCanPlayIfNotCorrectGenderReturnsFalse() {
        $this->beforeEach();

        $this->tournament->setGenders([ 'Male' ]);

        $result = $this->tournamentService->canPlay($this->player, $this->tournament);
        $this->assertFalse($result);
    }

    public function testCanPlayIfNotCorrectAgeFalse() {
        $this->beforeEach();

        $this->tournament->setCategories([ 'Junior' ]);

        $result = $this->tournamentService->canPlay($this->player, $this->tournament);
        $this->assertFalse($result);
    }
}