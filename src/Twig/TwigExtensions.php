<?php

namespace App\Twig;

use App\Entity\MatchResult;
use App\Entity\Player;
use App\Service\TournamentService;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Extension\AbstractExtension;
use Twig\Markup;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Symfony\Component\Filesystem\Path;

class TwigExtensions extends AbstractExtension
{
    /** @var TournamentService */
    private $tournamentService;

    /** @var ContainerBagInterface */
    private $params;

    public function __construct(TournamentService $tournamentService, ContainerBagInterface $params)
    {
        $this->tournamentService = $tournamentService;
        $this->params = $params;
    }

    public function getFilters() : array
    {
        return [
            new TwigFilter('implodeEnum', [$this, 'implodeEnum']),
            new TwigFilter('implode', [$this, 'implode']),
            new TwigFilter('playerImg', [$this, 'playerImg']),
            new TwigFilter('playerName', [$this, 'playerName']),
        ];
    }

    public function getFunctions() : array
    {
        return [
            new TwigFunction('canPlay', [$this->tournamentService, 'canPlay']),
            new TwigFunction('canLeave', [$this->tournamentService, 'canLeave']),
            new TwigFunction('canStart', [$this->tournamentService, 'canStart']),
            new TwigFunction('canNextRound', [$this->tournamentService, 'canNextRound']),
            new TwigFunction('canUpdate', [$this->tournamentService, 'canUpdate']),
            new TwigFunction('optionsEnum', [$this, 'optionsEnum']),
        ];
    }

    public function implodeEnum(Array $array, string $separator = ' ') : string {
        return implode($separator, array_map(function($item){
            return $item->value;
        },$array));
    }

    public function implode(Array $array, string $separator = ' ') : string {
        return implode($separator, $array);
    }

    public function playerImg(?string $path) {
        $source = null;
        if($path) {
            $source =  Path::join($this->params->get('web_player_img_dir'), $path);
        } else {
            $source = Path::join($this->params->get('web_default_img_dir'), 'player.png');
        }
        return new Markup(sprintf('<img src="%s">', $source), 'UTF-8');
    }

    public function playerName(?Player $player) {
        $playerName = '-';
        if($player) {
            $playerName = $player->getFirstName() . ' ' . $player->getLastName();
        }
        return new Markup(sprintf('<span class="playerMatch">%s</span>', $playerName), 'UTF-8');
    }

    public function optionsEnum(string $namespace, $selected = null, $excluded = [])
    {
        $enum = new ReflectionClass($namespace);
        $constants = $enum->getConstants();
        $options = '';
        foreach ($constants as $constant) {
            if(in_array($constant->name, $excluded)) {
                continue;
            }
            if($constant === $selected) {
                $options .= sprintf('<option selected value="%s">%s</option>', $constant->value, $constant->value);
            }
            else {
                $options .= sprintf('<option value="%s">%s</option>', $constant->value, $constant->value);
            }
        }
        return new Markup($options, 'UTF-8');
    }

}