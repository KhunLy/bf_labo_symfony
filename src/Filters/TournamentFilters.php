<?php

namespace App\Filters;

use Symfony\Component\Validator\Constraints as Assert;

class TournamentFilters
{
    /** @var ?string */
    private $name;

    /** @var boolean */
    private $closed = false; // valeur par défaut

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return TournamentFilters
     */
    public function setName(?string $name): TournamentFilters
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->closed;
    }

    /**
     * @param bool $closed
     * @return TournamentFilters
     */
    public function setClosed(bool $closed): TournamentFilters
    {
        $this->closed = $closed;
        return $this;
    }
}