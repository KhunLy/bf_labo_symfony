<?php

namespace App\Service;

use App\Entity\MatchResult;
use App\Entity\Player;
use App\Entity\Tournament;
use App\Entity\TournamentCategory;
use App\Entity\TournamentMatch;
use App\Repository\TournamentMatchRepository;
use App\Repository\TournamentRepository;

class TournamentService
{
    /** @var TournamentRepository */
    private $tournamentRepository;

    /** @var TournamentMatchRepository */
    private $matchRepository;

    public function __construct(
        TournamentRepository $tournamentRepository,
        TournamentMatchRepository $matchRepository,
    )
    {
        $this->tournamentRepository = $tournamentRepository;
        $this->matchRepository = $matchRepository;
    }

    /**
     * Checks if the player can play in the tournament
     * @param Player|null $player
     * @param Tournament $tournament
     * @return bool
     */
    public function canPlay(?Player $player, Tournament $tournament) : bool {
        // first check if player !== null
        if(!$player){
            return false;
        }
        // check if tournament is not full
        if(count($tournament->getPlayers()) >= $tournament->getMax()) {
            return false;
        }
        // check if tournament already contains player
        if(in_array($player, $tournament->getPlayers()->toArray())) {
            return false;
        }
        // check date
        if(new \DateTime() > $tournament->getDate()) {
            return false;
        }
        // check ELO
        if(($tournament->getEloMin() && $player->getElo() < $tournament->getEloMin()) || ($tournament->getEloMax() && $player->getElo() > $tournament->getEloMax())) {
            return false;
        }
        // check player gender
        if(!in_array($player->getGender(), $tournament->getGenders())) {
            return false;
        }
        // check player age
        // si aucune des categories du tournoi n'accepte l'age du joueur
        if(empty(array_filter($tournament->getCategories(), function ($category) use($player, $tournament) {
            // age du joueur au moment du tournoi
            $age = $this->calculateAge($player, $tournament);
            switch ($category) {
                case TournamentCategory::JUNIOR:
                    return $age >= 0 && $age < 18;
                case TournamentCategory::SENIOR:
                    return $age >= 18 && $age < 60;
                case TournamentCategory::VETERAN:
                    return $age >= 60;
                default:
                    return false;
            }
        }))) {
            return false;
        }
        return true;
    }

    /**
     * Checks if the player can play in the tournament
     * @param Player|null $player
     * @param Tournament $tournament
     * @return bool
     */
    public function canLeave(?Player $player, Tournament $tournament) : bool {
        // first check if player !== null
        if(!$player){
            return false;
        }
        // check if tournament has not start
        if($tournament->getCurrentRound() > 0) {
            return false;
        }
        // check if tournament contains player
        if(!in_array($player, $tournament->getPlayers()->toArray())) {
            return false;
        }
        return true;
    }

    /**
     * Check if tournament can start
     * @param ?Player $player
     * @param Tournament $tournament
     * @return bool
     */
    public function canStart(?Player $player, Tournament $tournament) : bool
    {
        if(!$this->isAdmin($player)) {
            return false;
        }
        if($tournament->getCurrentRound() > 0) {
            return false;
        }
        return /*(new \DateTime()) > $tournament->getDate() &&*/ Count($tournament->getPlayers()) >= $tournament->getMin();
    }

    /**
     * Check if tournament can go to the next round
     * @param ?Player $player
     * @param Tournament $tournament
     * @return bool
     */
    public function canNextRound(?Player $player, Tournament $tournament)
    {
        if(!$this->isAdmin($player)) {
            return false;
        }
        if($tournament->getMaxRound() < $tournament->getCurrentRound()) {
            return false;
        }
        return $this->matchRepository->count([
            'tournament' => $tournament->getId(),
            'round' => $tournament->getCurrentRound(),
            'result' => MatchResult::NOT_PLAYED->value
        ]) === 0;
    }


    /**
     * Check if a match can be updated
     * @param Player|null $player
     * @param TournamentMatch $match
     * @param Tournament $tournament
     * @return bool
     */
    public function canUpdate(?Player $player, TournamentMatch $match, Tournament $tournament)
    {
        if(!$this->isAdmin($player)) {
            return false;
        }
        if($match->getRound() != $tournament->getCurrentRound()) {
            return false;
        }
        if($match->getResult() === MatchResult::BYE) {
            return false;
        }
        return true;
    }

    /**
     * Calculate player age the day of tournament date
     * @param Player $player
     * @param Tournament $tournament
     * @return int
     */
    public function calculateAge(Player $player, Tournament $tournament) : int {
        return date_diff($tournament->getDate(), $player->getBirthDate())->y;
    }

    private function isAdmin(?Player $player) : bool {
        if(!$player){
            return false;
        }
        if(!in_array('ROLE_ADMIN', $player->getRoles())) {
            return false;
        }
        return true;
    }
}