<?php

namespace App\Controller;

use App\Entity\MatchResult;
use App\Entity\Player;
use App\Entity\Tournament;
use App\Entity\TournamentMatch;
use App\Filters\TournamentFilters;
use App\Form\TournamentAddType;
use App\Form\TournamentFiltersType;
use App\Repository\PlayerScoreRepository;
use App\Repository\TournamentRepository;
use App\Service\TournamentService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class TournamentController extends AbstractController
{
    /** @var TournamentRepository  */
    private $tournamentRepo;

    /** @var TournamentService */
    private $tournamentService;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        EntityManagerInterface $em,
        TournamentRepository $tournamentRepo,
        TournamentService $tournamentService,
    )
    {
        $this->em = $em;
        $this->tournamentRepo = $tournamentRepo;
        $this->tournamentService = $tournamentService;
    }

    #[Route('/', name: 'tournament_index')]
    public function index(Request $request): Response
    {
        $filters = new TournamentFilters();
        $form = $this->createForm(TournamentFiltersType::class, $filters);
        $form->handleRequest($request);

        $tournaments = $this->tournamentRepo->findByFilters($filters);

        return $this->render('tournament/index.html.twig', [
            'form' => $form->createView(),
            'tournaments' => $tournaments
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/tournament/add', name: 'tournament_add')]
    public function add(Request $request): Response
    {
        $tournament = new Tournament();
        $form = $this->createForm(TournamentAddType::class, $tournament);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $tournament->setCurrentRound(0);
            $this->em->persist($tournament);
            $this->em->flush();
            $this->addFlash('success', 'Nouveau tournoi crée');
            return $this->redirectToRoute('tournament_index');
        }

        return $this->render('tournament/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED')]
    #[Route('/tournament/participate/{tournamentId}', name: 'tournament_participate')]
    public function participate(int $tournamentId)
    {
        $tournament = $this->tournamentRepo->findOneWithPlayers($tournamentId);
        if(!$tournament) {
            throw new NotFoundHttpException();
        }
        /** @var Player $player */
        $player = $this->getUser();
        if($this->tournamentService->canPlay($player, $tournament))
        {
            $tournament->addPlayer($player);
            $this->em->flush();
            $this->addFlash('success', sprintf(
                    'Vous participez au tournoi %s',
                    $tournament->getName()
                )
            );
        }
        else {
            $this->addFlash('error', sprintf(
                    'Vous ne pouvez pas participer au tournoi %s',
                    $tournament->getName()
                )
            );
        }
        return $this->redirectToRoute('tournament_index');
    }

    #[IsGranted('IS_AUTHENTICATED')]
    #[Route('/tournament/leave/{tournamentId}', name: 'tournament_leave')]
    public function leave(int $tournamentId)
    {
        $tournament = $this->tournamentRepo->findOneWithPlayers($tournamentId);
        if(!$tournament) {
            throw new NotFoundHttpException();
        }
        /** @var Player $player */
        $player = $this->getUser();
        if($this->tournamentService->canLeave($player, $tournament))
        {
            $tournament->removePlayer($player);
            $this->em->flush();
            $this->addFlash('success', sprintf(
                    'Vous ne participez plus au tournoi %s',
                    $tournament->getName()
                )
            );
        }
        else {
            $this->addFlash('error', sprintf(
                    'Vous ne pouvez pas quitter le tournoi %s',
                    $tournament->getName()
                )
            );
        }
        return $this->redirectToRoute('tournament_index');
    }

    #[Route('/tournament/details/{tournamentId}/{round}', name: 'tournament_details')]
    public function details(int $tournamentId, $round, PlayerScoreRepository $playerScoreRepository)
    {
        $tournament = $this->tournamentRepo->findWithMatches($tournamentId, $round);
        $players = $playerScoreRepository->findPlayersScore($tournamentId, $round);
        if(!$tournament || (count($players) < $round && $round != 0)) {
            //throw new NotFoundHttpException();
        }

        return $this->render('tournament/details.html.twig', [
            'tournament' => $tournament,
            'players' => $players,
            'round' => $round,
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/tournament/start/{tournamentId}', name: 'tournament_start')]
    public function start(int $tournamentId): Response
    {
        $tournament = $this->tournamentRepo->findOneWithPlayers($tournamentId);
        if(!$tournament) {
            throw new NotFoundHttpException();
        }

        /** @var Player $player */
        $player = $this->getUser();

        if($this->tournamentService->canStart($player, $tournament)) {
            $tournament->setCurrentRound(1);
            $players = $tournament->players->toArray();
            $count = count($players);

            // number of players is null add a "null player"
            if($count % 2 !== 0) {
                $players[] = null;
                $count++;
            }

            for($r=1; $r<$count; $r++) {
                for($i=0;$i < $count/2; $i++) {
                    $m = new TournamentMatch();
                    $m->setWhite($players[$i]);
                    $m->setBlack($players[$count - 1 - $i]);
                    $m->setRound($r);
                    $m->setTournament($tournament);
                    $m->setResult(MatchResult::NOT_PLAYED);
                    $this->em->persist($m);

                }
                $players = array_merge(
                    [$players[0], $players[$count-1]],
                    array_slice($players, 1, $count-2)
                );
            }
            $this->em->flush();
        }
        else {
            $this->addFlash('error', 'Vous ne pouvez pas démarrer ce tournoi');
        }
        return $this->redirectToRoute('tournament_details', [
            'tournamentId' => $tournamentId,
            'round' => $tournament->getCurrentRound()
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/tournament/update/{tournament}/{match}/{result}', name: 'tournament_match_update')]
    public function updateResult(Tournament $tournament, TournamentMatch $match, $result): Response
    {
        $result = MatchResult::tryFrom($result);
        if(!$result) {
            throw new NotFoundHttpException();
        }
        /** @var $player Player */
        $player = $this->getUser();
        if($this->tournamentService->canUpdate($player, $match, $tournament)) {
            $match->setResult($result);
            $this->em->flush();
            $this->addFlash('success', 'OK');
        }
        else {
            $this->addFlash('error', 'Vous ne pouvez pas mettre à jour ce match');
        }
        return $this->redirectToRoute('tournament_details', [
            'tournamentId' => $tournament->getId(),
            'round' => $tournament->getCurrentRound()
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/tournament/next/{tournament}', name: 'tournament_next')]
    public function next(Tournament $tournament): Response
    {
        /** @var $player Player */
        $player = $this->getUser();
        if($this->tournamentService->canNextRound($player, $tournament)) {
            $tournament->setCurrentRound($tournament->getCurrentRound() + 1);
            $this->em->flush();
            $this->addFlash('success', 'OK');
        }
        else {
            $this->addFlash('error', 'Vous ne pouvez pas terminer cette ronde');
        }
        return $this->redirectToRoute('tournament_details', [
            'tournamentId' => $tournament->getId(),
            'round' => $tournament->getCurrentRound()
        ]);
    }
}
