<?php

namespace App\Controller;

use App\Entity\Player;
use App\Form\RegisterType;
use App\Repository\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class PlayerController extends AbstractController
{
    /** @var PlayerRepository  */
    private $playerRepo;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        EntityManagerInterface $em,
        PlayerRepository $playerRepo
    )
    {
        $this->em = $em;
        $this->playerRepo = $playerRepo;
    }

    #[Route('/player', name: 'player_index')]
    public function index(): Response
    {
        return $this->render('player/index.html.twig', [
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED')]
    #[Route('/player/profile', name: 'player_profile')]
    public function profile(): Response
    {
        return $this->render('player/profile.html.twig', [
        ]);
    }

    #[Route('/player/register', name: 'player_register')]
    public function register(Request $request, UserPasswordHasherInterface $hasher, ContainerBagInterface $params): Response
    {
        $player = new Player();
        $form = $this->createForm(RegisterType::class, $player);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $player->setRole('ROLE_USER');
            $player->setPassword($hasher->hashPassword($player, $form->get('plainPassword')->getData()));
            /** @var $file UploadedFile */
            if($file = $form->get('file')->getData()) {
                $fileName = uniqid() . '.' . $file->getClientOriginalExtension();
                $file->move($params->get('player_img_dir'), $fileName);
                $player->setImage($fileName);
            }
            $this->em->persist($player);
            $this->em->flush();
            $this->addFlash('success', 'Inscription OK');
            return $this->redirectToRoute('app_login');
        }

        return $this->render('player/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
