<?php

namespace App\Repository;

use App\Entity\PlayerScore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayerScore|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayerScore|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayerScore[]    findAll()
 * @method PlayerScore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayerScoreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayerScore::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(PlayerScore $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(PlayerScore $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return PlayerScore[] Returns an array of PlayerScore objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayerScore
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * @param $tournamentId
     * @param $round
     * @return array
     */
    public function findPlayersScore($tournamentId, $round) : array
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(PlayerScore::class, 'ps');
        $rsm->addFieldResult('ps', 'id', 'id');
        $rsm->addFieldResult('ps', 'last_name', 'lastName');
        $rsm->addFieldResult('ps', 'first_name', 'firstName');
        $rsm->addFieldResult('ps', 'image', 'image');
        $rsm->addFieldResult('ps', 'victories', 'victories');
        $rsm->addFieldResult('ps', 'defeats', 'defeats');
        $rsm->addFieldResult('ps', 'ties', 'ties');
        $rsm->addFieldResult('ps', 'score', 'score');
        $query = $this->getEntityManager()->createNativeQuery('CALL SP_PLAYERS_SCORE(:p1, :p2)', $rsm);
        $query->setParameter('p1', $tournamentId);
        $query->setParameter('p2', $round);
        return  $query->getResult();
    }
}
