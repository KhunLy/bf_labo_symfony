<?php

namespace App\Repository;

use App\Entity\MatchResult;
use App\Entity\Player;
use App\Entity\PlayerScore;
use App\Entity\Tournament;
use App\Filters\TournamentFilters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tournament|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tournament|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tournament[]    findAll()
 * @method Tournament[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TournamentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tournament::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Tournament $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Tournament $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Tournament[] Returns an array of Tournament objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tournament
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /** @return Collection<Tournament> */
    public function findByFilters(TournamentFilters $filters)
    {
        $qb = $this->createQueryBuilder('t')
            // join players
            ->leftJoin('t.players', 'p')
            ->addSelect('p')
            ->where('t.name LIKE :p1')
            ->setParameter('p1', $filters->getName() . '%');
        // ajouter les matchs des tournois pour verifier que tous les matches ont été joués

        $qb->leftJoin('t.matches', 'm', Join::WITH, 'm.result = :p2');
        $qb->setParameter('p2', MatchResult::NOT_PLAYED->value);

        // la condition change en fonction du filtre
        $qb->groupBy('t,p');
        if($filters->isClosed()) {
            $qb->having('COUNT(t.id) = 0');
        } else {
            $qb->having('COUNT(t.id) > 0');
        }

        $qb->orderBy('t.date');
        return $qb->getQuery()->getResult();
    }

    /**
     * @param $tournamentId
     * @param $round
     * @return ?Tournament
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findWithMatches($tournamentId, $round) : ?Tournament
    {
        $qb = $this->createQueryBuilder('t');
        $qb->where('t.id = :p1');
        $qb->setParameter('p1', $tournamentId);
        $qb->leftJoin('t.matches', 'm', Join::WITH, 'm.round = :p2');
        $qb->setParameter('p2', $round);
        $qb->leftJoin('m.black', 'pb');
        $qb->leftJoin('m.white', 'pw');
        $qb->addSelect('m');
        $qb->addSelect('pb');
        $qb->addSelect('pw');

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findOneWithPlayers(int $id) : ?Tournament
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.id = :p1')
            ->setParameter('p1', $id)
            ->leftJoin('t.players', 'p')
            ->addSelect('p');

        return $qb->getQuery()->getOneOrNullResult();
    }
}
