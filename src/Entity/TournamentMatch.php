<?php

namespace App\Entity;

use App\Repository\TournamentMatchRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TournamentMatchRepository::class)]
class TournamentMatch
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    #[ORM\Column(type: 'integer')]
    private $round;

    #[ORM\Column(type: 'string', enumType: MatchResult::class)]
    private $result;

    #[ORM\ManyToOne(targetEntity: Tournament::class, inversedBy: 'matches')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')] // remove match if the Tournament is deleted
    private $tournament;

    #[ORM\ManyToOne(targetEntity: Player::class)]
    #[ORM\JoinColumn(nullable: true)]
    private $white;

    #[ORM\ManyToOne(targetEntity: Player::class)]
    #[ORM\JoinColumn(nullable: true)]
    private $black;

    public function getRound(): ?int
    {
        return $this->round;
    }

    public function setRound(int $round): self
    {
        $this->round = $round;

        return $this;
    }

    public function getResult(): MatchResult
    {
        return $this->result;
    }

    public function setResult(MatchResult $result): self
    {
        $this->result = $result;

        return $this;
    }

    public function getTournament(): ?Tournament
    {
        return $this->tournament;
    }

    public function setTournament(?Tournament $tournament): self
    {
        $this->tournament = $tournament;

        return $this;
    }

    public function getWhite(): ?Player
    {
        return $this->white;
    }

    public function setWhite(?Player $white): self
    {
        $this->white = $white;

        return $this;
    }

    public function getBlack(): ?Player
    {
        return $this->black;
    }

    public function setBlack(?Player $black): self
    {
        $this->black = $black;

        return $this;
    }
}
