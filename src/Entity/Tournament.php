<?php

namespace App\Entity;

use App\Repository\TournamentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: TournamentRepository::class)]
class Tournament
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\Length(max: 50)]
    #[ORM\Column(type: 'string', length: 50)]
    private $name;

    #[Assert\NotNull]
    #[Assert\GreaterThanOrEqual('today')]
    #[ORM\Column(type: 'date')]
    private $date;

    #[Assert\NotBlank]
    #[Assert\Length(max: 100)]
    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private $location;

    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 3000)]
    #[ORM\Column(type: 'integer', nullable: true)]
    private $eloMin = 0;

    #[Assert\NotNull]
    #[Assert\Range(min: 0, max: 3000)]
    #[Assert\Expression('value >= this.getEloMin()')]
    #[ORM\Column(type: 'integer', nullable: true)]
    private $eloMax = 0;

    #[Assert\NotNull]
    #[ORM\Column(type: 'boolean')]
    private $ranked = true; // default value

    #[Assert\NotNull]
    #[Assert\Count(min: 1)]
    #[ORM\Column(type: 'json')] // used for array // mapped to enum in getter
    private $categories = []; // default value

    #[Assert\NotNull]
    #[Assert\Count(min: 1)]
    #[ORM\Column(type: 'json')] // used for array
    private $genders = [];

    #[Assert\NotNull]
    #[Assert\Range(min: 2, max: 100)]
    #[ORM\Column(type: 'integer')]
    private $min = 2;

    #[Assert\NotNull]
    #[Assert\Range(min: 2, max: 100)]
    #[Assert\Expression('value >= this.getMin()')]
    #[ORM\Column(type: 'integer')]
    private $max = 2;

    #[ORM\Column(type: 'integer')]
    private $currentRound;

    #[ORM\ManyToMany(targetEntity: Player::class)]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    public $players;

    #[ORM\OneToMany(targetEntity: TournamentMatch::class, mappedBy: 'tournament')]
    public $matches;

    public function __construct()
    {
        $this->players = new ArrayCollection();
        $this->matches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Tournament
     */
    public function setName(string $name) : Tournament
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate() : \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return Tournament
     */
    public function setDate(\DateTime $date) : Tournament
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     * @return Tournament
     */
    public function setLocation(string $location) : Tournament
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return ?int
     */
    public function getEloMin() : ?int
    {
        return $this->eloMin;
    }

    /**
     * @param ?int $eloMin
     * @return Tournament
     */
    public function setEloMin(?int $eloMin) : Tournament
    {
        $this->eloMin = $eloMin;
        return $this;
    }

    /**
     * @return ?int
     */
    public function getEloMax() : ?int
    {
        return $this->eloMax;
    }

    /**
     * @param ?int $eloMax
     * @return Tournament
     */
    public function setEloMax(?int $eloMax) : Tournament
    {
        $this->eloMax = $eloMax;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRanked(): bool
    {
        return $this->ranked;
    }

    /**
     * @param bool $ranked
     * @return Tournament
     */
    public function setRanked(bool $ranked): Tournament
    {
        $this->ranked = $ranked;
        return $this;
    }

    public function getRanked(): ?bool
    {
        return $this->ranked;
    }

    public function getCategories(): array
    {
        return array_map(function($item) {
            return TournamentCategory::from($item);
        }, $this->categories);
    }

    public function setCategories(array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function getGenders(): array
    {
        return array_map(function($item) {
            return Gender::from($item);
        }, $this->genders);
    }

    public function setGenders(array $genders): self
    {
        $this->genders = $genders;

        return $this;
    }

    /**
     * @return int
     */
    public function getMin() : int
    {
        return $this->min;
    }

    /**
     * @param int $min
     * @return Tournament
     */
    public function setMin(int $min) : self
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @return int
     */
    public function getMax() : int
    {
        return $this->max;
    }

    /**
     * @param int $max
     * @return Tournament
     */
    public function setMax(int $max) : self
    {
        $this->max = $max;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentRound() : int
    {
        return $this->currentRound;
    }

    /**
     * @param int $currentRound
     * @return Tournament
     */
    public function setCurrentRound(int $currentRound) : self
    {
        $this->currentRound = $currentRound;
        return $this;
    }

    /**
     * @return Collection<int, Player>
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Player $player): self
    {
        if (!$this->players->contains($player)) {
            $this->players[] = $player;
        }

        return $this;
    }

    public function removePlayer(Player $player): self
    {
        $this->players->removeElement($player);

        return $this;
    }

    /**
     * @return Collection<int, TournamentMatch>
     */
    public function getMatches(): Collection
    {
        return $this->matches;
    }

    public function addMatch(TournamentMatch $match): self
    {
        if (!$this->matches->contains($match)) {
            $this->matches[] = $match;
            $match->setTournament($this);
        }

        return $this;
    }

    public function removeMatch(TournamentMatch $match): self
    {
        if ($this->matches->removeElement($match)) {
            // set the owning side to null (unless already changed)
            if ($match->getTournament() === $this) {
                $match->setTournament(null);
            }
        }

        return $this;
    }

    public function getMaxRound() : int
    {
        if($this->players->count() % 2 === 0) {
            return $this->players->count() - 1;
        }
        return $this->players->count();
    }

}
