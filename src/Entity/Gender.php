<?php

namespace App\Entity;

enum Gender : string
{
    case MALE = 'Male';
    case FEMALE = 'Female';
}