<?php

namespace App\Form;

use App\Entity\Gender;
use App\Entity\Tournament;
use App\Entity\TournamentCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TournamentAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('date', DateType::class, [
                'label' => 'Date de fin des inscriptions',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('location', TextType::class, [
                'label' => 'Lieu',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('eloMin', RangeType::class, [
                'label' => 'Elo min',
                'required' => false,
                'attr' => [
                    'class' => 'form-range',
                    'min' => 0,
                    'max' => 3000,
                    'step' => 100,
                ]
            ])
            ->add('eloMax', RangeType::class, [
                'label' => 'Elo min',
                'required' => false,
                'attr' => [
                    'class' => 'form-range',
                    'min' => 0,
                    'max' => 3000,
                    'step' => 100,
                ]
            ])
            ->add('ranked', CheckboxType::class, [
                'label' => 'Classé',
                'label_attr' => ['class' => 'form-check-label'],
                'required' => false,
                'attr' => ['class' => 'form-check-input']
            ])
            ->add('categories', EnumType::class, [
                'class' => TournamentCategory::class,
                'label' => 'Categories',
                'multiple' => true,
                'attr' => ['class' => 'select2'],
            ])
            ->add('genders', EnumType::class, [
                'class' => Gender::class,
                'label' => 'Genres',
                'multiple' => true,
                'attr' => ['class' => 'select2'],
            ])
            ->add('min', RangeType::class, [
                'label' => 'Nombre minimum de joueurs',
                'attr' => [
                    'class' => 'form-range',
                    'min' => 2,
                    'max' => 100,
                ]
            ])
            ->add('max', RangeType::class, [
                'label' => 'Nombre maximum de joueurs',
                'attr' => [
                    'class' => 'form-range',
                    'min' => 2,
                    'max' => 100,
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tournament::class,
        ]);
    }
}
