<?php

namespace App\Form;

use App\Entity\Gender;
use App\Entity\TournamentCategory;
use App\Filters\TournamentFilters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TournamentFiltersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom du tournoi',
                'required' => false,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('closed', CheckboxType::class, [
                'label' => 'Terminé',
                'label_attr' => ['class' => 'form-check-label'],
                'required' => false,
                'attr' => ['class' => 'form-check-input']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TournamentFilters::class,
            // Configure your form options here
            // par défaut les formulaires sont valides s'ils sont soumis avec le verbe HTTP POST
            'method' => 'get',
            // désactiver l'antiforgery token
            'csrf_protection' => false,
        ]);
    }
}
