<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220327222639 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("
            CREATE PROCEDURE SP_PLAYERS_SCORE(IN p1 INT, IN p2 INT)
            SELECT
                p.id, p.last_name, p.first_name, p.image,
                SUM(CASE WHEN ((m.result = 'white' && m.white_id = p.id) OR (m.result = 'black' && m.black_id = p.id)) THEN 1 ELSE 0 END) AS victories,
                SUM(CASE WHEN ((m.result = 'black' && m.white_id = p.id) OR (m.result = 'white' && m.black_id = p.id)) THEN 1 ELSE 0 END) AS defeats,
                SUM(CASE WHEN m.result = 'tie' THEN 1 ELSE 0 END) AS ties,
                SUM(CASE 
                    WHEN m.result = 'tie' THEN 0.5
                    WHEN ((m.result = 'white' && m.white_id = p.id) OR (m.result = 'black' && m.black_id = p.id)) THEN 1
                    ELSE 0 END
                ) AS score
            FROM tournament t
                     JOIN tournament_player j ON j.tournament_id = t.id
                     LEFT JOIN player p on j.player_id = p.id
                     LEFT JOIN tournament_match m on (p.id = m.black_id OR p.id = m.white_id) AND m.round <= p2
            WHERE t.id = p1
            GROUP BY p.id, p.last_name, p.first_name, p.image
            ORDER BY score DESC, victories DESC, defeats ASC, p.last_name ASC 
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP PROCEDURE SP_PLAYERS_SCORE');
    }
}
