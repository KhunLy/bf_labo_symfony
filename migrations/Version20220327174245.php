<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220327174245 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE player (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, role VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_name VARCHAR(100) NOT NULL, first_name VARCHAR(100) NOT NULL, birth_date DATE NOT NULL, gender VARCHAR(255) NOT NULL, elo INT NOT NULL, image VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_98197A65E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, date DATE NOT NULL, location VARCHAR(100) DEFAULT NULL, elo_min INT DEFAULT NULL, elo_max INT DEFAULT NULL, ranked TINYINT(1) NOT NULL, categories LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', genders LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', min INT NOT NULL, max INT NOT NULL, current_round INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_player (tournament_id INT NOT NULL, player_id INT NOT NULL, INDEX IDX_FCB3843533D1A3E7 (tournament_id), INDEX IDX_FCB3843599E6F5DF (player_id), PRIMARY KEY(tournament_id, player_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tournament_match (id INT AUTO_INCREMENT NOT NULL, tournament_id INT DEFAULT NULL, white_id INT DEFAULT NULL, black_id INT DEFAULT NULL, round INT NOT NULL, result VARCHAR(255) NOT NULL, INDEX IDX_BB0D551C33D1A3E7 (tournament_id), INDEX IDX_BB0D551CCDBF46EC (white_id), INDEX IDX_BB0D551CD3E7E37C (black_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tournament_player ADD CONSTRAINT FK_FCB3843533D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_player ADD CONSTRAINT FK_FCB3843599E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_match ADD CONSTRAINT FK_BB0D551C33D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_match ADD CONSTRAINT FK_BB0D551CCDBF46EC FOREIGN KEY (white_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE tournament_match ADD CONSTRAINT FK_BB0D551CD3E7E37C FOREIGN KEY (black_id) REFERENCES player (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tournament_player DROP FOREIGN KEY FK_FCB3843599E6F5DF');
        $this->addSql('ALTER TABLE tournament_match DROP FOREIGN KEY FK_BB0D551CCDBF46EC');
        $this->addSql('ALTER TABLE tournament_match DROP FOREIGN KEY FK_BB0D551CD3E7E37C');
        $this->addSql('ALTER TABLE tournament_player DROP FOREIGN KEY FK_FCB3843533D1A3E7');
        $this->addSql('ALTER TABLE tournament_match DROP FOREIGN KEY FK_BB0D551C33D1A3E7');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE tournament');
        $this->addSql('DROP TABLE tournament_player');
        $this->addSql('DROP TABLE tournament_match');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
